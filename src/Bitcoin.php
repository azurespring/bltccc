<?php

namespace AzureSpring\Bltccc;

use AzureSpring\Bltccc\Exception\NotImplementedException;
use AzureSpring\Bltccc\Model\FundRawTransactionOptions;
use AzureSpring\Bltccc\Model\Payment;
use AzureSpring\Bltccc\Model\ReceivedByAddress;
use AzureSpring\Bltccc\Model\Transaction;
use AzureSpring\Bltccc\Model\UnspentOutput;
use AzureSpring\Bltccc\Model\WalletInfo;
use GuzzleHttp\Client;

class Bitcoin implements BitcoinInterface
{
    private $client;


    /**
     * Construct
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @inheritdoc
     */
    public function getWalletInfo(): WalletInfo
    {
        return new WalletInfo($this->call('getwalletinfo'));
    }

    /**
     * @inheritDoc
     */
    public function encryptWallet(string $passphrase)
    {
        throw new NotImplementedException();
    }

    /**
     * @inheritDoc
     */
    public function walletLock()
    {
        $this->call('walletlock');
    }

    /**
     * @inheritDoc
     */
    public function walletPassphrase(string $passphrase, int $timeout)
    {
        $this->call('walletpassphrase', [ $passphrase, $timeout ]);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function walletPassphraseChange(string $oldPassphrase, string $newPassphrase)
    {
        $this->call('walletpassphrasechange', [ $oldPassphrase, $newPassphrase ]);
    }

    /**
     * @inheritdoc
     */
    public function keyPoolRefill(int $size = 100)
    {
        $this->call('keypoolrefill', [ $size ]);
    }

    /**
     * @inheritdoc
     */
    public function setTxFee(float $amount)
    {
        $this->call('settxfee', [ $amount ]);
    }

    /**
     * @inheritdoc
     */
    public function getAccount(string $address): string
    {
        return $this->call('getaccount', [ $address ]);
    }

    /**
     * @inheritdoc
     */
    public function getBalance(string $account, int $minconf = 1, bool $includeWatchOnly = false): float
    {
        return $this->call('getbalance', [ $account, $minconf, $includeWatchOnly ]);
    }

    /**
     * @inheritdoc
     */
    public function getUnconfirmedBalance(): float
    {
        return $this->call('getunconfirmedbalance');
    }

    /**
     * @inheritdoc
     */
    public function getNewAddress(string $account): string
    {
        return $this->call('getnewaddress', [ $account ]);
    }

    /**
     * @inheritdoc
     */
    public function getReceivedByAddress(string $address, int $minconf = 1): float
    {
        return $this->call('getreceivedbyaddress', [ $address, $minconf ]);
    }

    /**
     * @inheritdoc
     */
    public function importAddress(string $address, string $account = '', bool $rescan = true, bool $p2sh = false)
    {
        return $this->call('importaddress', [ $address, $account, $rescan, $p2sh ]);
    }

    /**
     * @inheritdoc
     */
    public function importMulti($requests, $options)
    {
        throw new NotImplementedException();
    }

    /**
     * @inheritdoc
     */
    public function importPubKey(string $pubkey, string $label = '', bool $rescan = true)
    {
        throw new NotImplementedException();
    }

    /**
     * @inheritdoc
     */
    public function importPrivKey(string $litecoinPrivKey, string $account = '', bool $rescan = true)
    {
        return $this->call('importprivkey', [ $litecoinPrivKey, $account, $rescan ]);
    }

    /**
     * @inheritdoc
     */
    public function dumpPrivKey(string $address): string
    {
        return $this->call('dumpprivkey', [ $address ]);
    }

    /**
     * @inheritdoc
     */
    public function listAddressGroupings()
    {
        throw new NotImplementedException();
    }

    /**
     * @inheritdoc
     */
    public function listReceivedByAddress(int $minconf = 1, bool $includeEmpty = false, bool $includeWatchOnly = false): array
    {
        return array_map(
            function ($address) {
                return new ReceivedByAddress($address);
            },
            $this->call('listreceivedbyaddress', [ $minconf, $includeEmpty, $includeWatchOnly ])
        );
    }

    /**
     * @inheritdoc
     */
    public function listSinceBlock(string $blockHash = '', int $targetConfirmations = 1, bool $includeWatchOnly = false): array
    {
        return array_map(
            function ($payment) {
                return new Payment($payment);
            },
            $this->call('listsinceblock', [ $blockHash, $targetConfirmations, $includeWatchOnly ])->transactions
        );
    }

    /**
     * @inheritdoc
     */
    public function listTransactions(string $account, int $count = 10, int $skip = 0, bool $includeWatchOnly = false): array
    {
        return array_map(
            function ($tx) {
                return new Payment($tx);
            },
            $this->call('listtransactions', [ $account, $count, $skip, $includeWatchOnly ])
        );
    }

    /**
     * @inheritdoc
     */
    public function listUnspent(int $minconf = 1, int $maxconf = 9999999, array $addresses = null, $includeUnsafe = true): array
    {
        return array_map(
            function ($output) {
                return new UnspentOutput($output);
            },
            $this->call('listunspent', [ $minconf, $maxconf, $addresses, $includeUnsafe ])
        );
    }

    /**
     * @inheritdoc
     */
    public function listLockUnspent()
    {
        throw new NotImplementedException();
    }

    /**
     * @inheritdoc
     */
    public function lockUnspent(bool $unlock, array $transactions = null)
    {
        throw new NotImplementedException();
    }

    /**
     * @inheritdoc
     */
    public function sendMany(string $fromAccount, $outputs, int $minconf = 1, string $comment = null, array $subtractFeeFrom = null)
    {
        throw new NotImplementedException();
    }

    /**
     * @inheritdoc
     */
    public function sendToAddress(string $address, float $amount, string $comment = null, string $commentTo = null, bool $subtractFeeFromAmount = false): string
    {
        return $this->call('sendtoaddress', [ $address, $amount, $comment, $commentTo, $subtractFeeFromAmount ]);
    }

    /**
     * @inheritdoc
     */
    public function getTransaction(string $txId, bool $includeWatchOnly = false): Transaction
    {
        return new Transaction($this->call('gettransaction', [ $txId, $includeWatchOnly ]));
    }

    /**
     * @inheritdoc
     */
    public function importPrunedFunds($rawTransaction, $txOutProof)
    {
        throw new NotImplementedException();
    }

    /**
     * @inheritdoc
     */
    public function removePrunedFunds($txId)
    {
        throw new NotImplementedException();
    }

    /**
     * @inheritdoc
     */
    public function signMessage($address, $message)
    {
        throw new NotImplementedException();
    }

    /**
     * @inheritDoc
     */
    public function createRawTransaction(array $inputs, array $outputs, int $lockTime = null): string
    {
        return $this->call('createrawtransaction', [ $inputs, $outputs, $lockTime ]);
    }

    /**
     * @inheritDoc
     */
    public function decodeRawTransaction(string $tx)
    {
        return $this->call('decoderawtransaction', [ $tx ]);
    }

    /**
     * @inheritDoc
     */
    public function fundRawTransaction(string $tx, FundRawTransactionOptions $options = null): array
    {
        $params = [ $tx ];
        if ($options)
            $params[] = $options->marshal();
        $result = $this->call('fundrawtransaction', $params);

        return [ $result->hex, $result->fee, $result->changepos ];
    }

    /**
     * @inheritDoc
     */
    public function signRawTransaction(string $tx, array $dependencies = null, array $privateKeys = null, string $sigHash = null): array
    {
        $result = $this->call('signrawtransaction', [ $tx, $dependencies, $privateKeys, $sigHash ]);

        return [ $result->hex, $result->complete ];
    }

    /**
     * @inheritDoc
     */
    public function sendRawTransaction(string $tx, bool $allowHighFee = false): ?string
    {
        return $this->call('sendrawtransaction', [ $tx, $allowHighFee ]);
    }

    /**
     * @inheritdoc
     */
    public function validateAddress(string $address)
    {
        return $this->call('validateaddress', [ $address ])->isvalid;
    }

    private function call(string $method, array $params = [])
    {
        $id = uniqid();
        $response = json_decode(
            $this
            ->client
            ->post('', [
                'headers' => [
                    'content-type' => 'text/plain',
                ],
                'json' => [
                    'id' => $id,
                    'jsonrpc' => '1.0',
                    'method' => $method,
                    'params' => $params,
                ],
            ])
            ->getBody()
            ->getContents()
        );
        if ($response->id !== $id) {
            throw new \UnexpectedValueException();
        }
        if ($response->error) {
            switch ($response->error->code) {
                case self::RPC_VERIFY_ALREADY_IN_CHAIN:
                    throw new Exception\TxInChain($response->error->message, $response->error->code);

                default:
                    throw new \RuntimeException($response->error->message, $response->error->code);
            }
        }

        return $response->result;
    }
}

<?php

namespace AzureSpring\Bltccc\Model;

class TransactionDetail
{
    private $detail;


    /**
     * Construct
     *
     * @param object $detail
     */
    public function __construct(object $detail)
    {
        $this->detail = $detail;
    }

    /**
     * Set to true if the input or output involves a watch-only address
     *
     * @return bool|null
     */
    public function isInvolvesWatchOnly(): ?bool
    {
        return @$this->detail->involvesWatchonly;
    }

    /**
     * The account which the payment was credited to or debited from
     *
     * @return string
     */
    public function getAccount(): string
    {
        return $this->detail->account;
    }

    /**
     * If an output, the address paid (may be someone else’s address not belonging to this wallet);
     * if an input, the address paid in the previous output
     *
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return @$this->detail->address;
    }

    /**
     * @return string send, receive, generate, immature or orphan
     */
    public function getCategory(): string
    {
        return $this->detail->category;
    }

    /**
     * A negative bitcoin amount if sending payment; a positive bitcoin amount if receiving payment
     *
     * @return float
     */
    public function getAmount(): float
    {
        return $this->detail->amount;
    }

    /**
     * For an output, the output index (vout) for this output in this transaction
     *
     * @return int
     */
    public function getVOut(): int
    {
        return $this->detail->vout;
    }

    /**
     * If sending payment, the fee paid as a negative bitcoins value
     *
     * @return float|null
     */
    public function getFee(): ?float
    {
        return @$this->detail->fee;
    }

    /**
     * Indicates if a transaction is was abandoned
     *
     * @return bool|null
     */
    public function isAbandoned(): ?bool
    {
        return @$this->detail->abandoned;
    }
}

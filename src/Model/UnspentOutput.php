<?php

namespace AzureSpring\Bltccc\Model;

class UnspentOutput
{
    private $output;


    /**
     * Construct
     *
     * @param object $output
     */
    public function __construct(object $output)
    {
        $this->output = $output;
    }

    /**
     * The TXID of the transaction containing the output, encoded as hex in RPC byte order
     *
     * @return string
     */
    public function getTxId(): string
    {
        return $this->output->txid;
    }

    /**
     * The output index number (vout) of the output within its containing transaction
     *
     * @return int
     */
    public function getVOut(): int
    {
        return $this->output->vout;
    }

    /**
     * The P2PKH or P2SH address the output paid
     *
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return @$this->output->address;
    }

    /**
     * The output script paid, encoded as hex
     *
     * @return string
     */
    public function getScriptPubKey(): string
    {
        return $this->output->scriptPubKey;
    }

    /**
     * If the output is a P2SH whose script belongs to this wallet, this is the redeem script
     *
     * @return string|null
     */
    public function getRedeemScript(): ?string
    {
        return @$this->output->redeemScript;
    }

    /**
     * The amount paid to the output in bitcoins
     *
     * @return float
     */
    public function getAmount(): float
    {
        return $this->output->amount;
    }

    /**
     * The number of confirmations received for the transaction containing this output
     *
     * @return int
     */
    public function getConfirmations(): int
    {
        return $this->output->confirmations;
    }

    /**
     * Set to true if the private key or keys needed to spend this output are part of the wallet
     *
     * @return bool
     */
    public function getSpendable(): bool
    {
        return $this->output->spendable;
    }

    /**
     * Set to true if the wallet knows how to spend this output
     *
     * @return bool
     */
    public function getSolvable(): bool
    {
        return $this->output->solvable;
    }
}

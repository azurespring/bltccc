<?php

namespace AzureSpring\Bltccc\Model;

/**
 * Information about the wallet
 */
class WalletInfo
{
    private $info;


    /**
     * Construct
     *
     * @param object $info
     */
    public function __construct(object $info)
    {
        $this->info = $info;
    }

    /**
     * The version number of the wallet
     *
     * @return int
     */
    public function getVersion(): int
    {
        return $this->info->walletversion;
    }

    /**
     * The balance of the wallet
     *
     * @return float
     */
    public function getBalance(): float
    {
        return $this->info->balance;
    }

    /**
     * The total number of transactions in the wallet (both spends and receives)
     *
     * @return int
     */
    public function getTxCount(): int
    {
        return $this->info->txcount;
    }

    /**
     * The date as Unix epoch time when the oldest key in the wallet key pool was created
     *
     * @return int
     */
    public function getKeyPoolOldest(): int
    {
        return $this->info->keypoololdest;
    }

    /**
     * The number of keys in the wallet keypool
     *
     * @return int
     */
    public function getKeyPoolSize(): int
    {
        return $this->info->keypoolsize;
    }

    /**
     * Only returned if the wallet was encrypted with the encryptwallet RPC
     *
     * @return int a Unix epoch date when the wallet will be locked, or 0 if the wallet is currently locked
     */
    public function getUnlockedUntil(): ?int
    {
        return @$this->info->unlocked_until;
    }
}

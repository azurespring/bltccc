<?php

namespace AzureSpring\Bltccc\Model;

class Payment
{
    private $payment;


    /**
     * Construct
     *
     * @param object $payment
     */
    public function __construct(object $payment)
    {
        $this->payment = $payment;
    }

    /**
     * Set to true if the payment involves a watch-only address
     *
     * @return bool|null
     */
    public function involvesWatchOnly(): ?bool
    {
        return @$this->payment->involvesWatchonly;
    }

    /**
     * The account which the payment was credited to or debited from
     *
     * @deprecated
     *
     * @return string
     */
    public function getAccount(): string
    {
        return $this->payment->account;
    }

    /**
     * The address paid in this payment, which may be someone else’s address not belonging to this wallet
     *
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return @$this->payment->address;
    }

    /**
     * @return string send, receive, generate, immature, orphan and move
     */
    public function getCategory(): string
    {
        return $this->payment->category;
    }

    /**
     * A negative bitcoin amount if sending payment; a positive bitcoin amount if receiving payment
     *
     * @return float
     */
    public function getAmount(): float
    {
        return $this->payment->amount;
    }

    /**
     * A comment for the address/transaction
     *
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return @$this->payment->label;
    }

    /**
     * For an output, the output index (vout) for this output in this transaction
     *
     * @return int|null
     */
    public function getVOut(): ?int
    {
        return @$this->payment->vout;
    }

    /**
     * If sending payment, the fee paid as a negative bitcoins value
     *
     * @return float|null
     */
    public function getFee(): ?float
    {
        return @$this->payment->fee;
    }

    /**
     * The number of confirmations the transaction has received
     *
     * @return int|null
     */
    public function getConfirmations(): ?int
    {
        return @$this->payment->confirmations;
    }

    /**
     * Indicates whether we consider the outputs of this unconfirmed transaction safe to spend
     *
     * @return bool
     */
    public function isTrusted(): ?bool
    {
        return @$this->payment->trusted;
    }

    /**
     * Set to true if the transaction is a coinbase
     *
     * @return bool|null
     */
    public function isGenerated(): ?bool
    {
        return @$this->payment->generated;
    }

    /**
     * The hash of the block on the local best block chain which includes this transaction,
     * encoded as hex in RPC byte order
     *
     * @return string|null
     */
    public function getBlockHash(): ?string
    {
        return @$this->payment->blockhash;
    }

    /**
     * The index of the transaction in the block that includes it
     *
     * @return int|null
     */
    public function getBlockIndex(): ?int
    {
        return @$this->payment->blockindex;
    }

    /**
     * The block header time (Unix epoch time) of the block on the local best block chain which
     * includes this transaction
     *
     * @return int|null
     */
    public function getBlockTime(): ?int
    {
        return @$this->payment->blocktime;
    }

    /**
     * The TXID of the transaction, encoded as hex in RPC byte order
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return @$this->payment->txid;
    }

    /**
     * An array containing the TXIDs of other transactions that spend the same inputs (UTXOs) as
     * this transaction
     *
     * @return string[]|null
     */
    public function getWalletConflicts(): ?array
    {
        return @$this->payment->walletconflicts;
    }

    /**
     * A Unix epoch time when the transaction was added to the wallet
     *
     * @return int
     */
    public function getTime(): int
    {
        return $this->payment->time;
    }

    /**
     * A Unix epoch time when the transaction was detected by the local node, or
     * the time of the block on the local best block chain that included the transaction
     *
     * @return int|null
     */
    public function getTimeReceived(): ?int
    {
        return @$this->payment->timereceived;
    }

    /**
     * For transaction originating with this wallet, a locally-stored comment added to the transaction
     *
     * @return string|null
     */
    public function getComment(): ?string
    {
        return @$this->payment->comment;
    }

    /**
     * For transaction originating with this wallet, a locally-stored comment added to
     * the transaction identifying who the transaction was sent to
     *
     * @return string|null
     */
    public function getTo(): ?string
    {
        return @$this->payment->to;
    }

    /**
     * This is the account the bitcoins were moved from or moved to, as indicated by
     * a negative or positive amount field in this payment
     *
     * @return string|null
     */
    public function getOtherAccount(): ?string
    {
        return @$this->payment->otheraccount;
    }

    /**
     * Indicates if a transaction is replaceable under BIP125
     *
     * Has nothing to do with Bitcoin Cash (null).
     *
     * @return string yes, no or unknown
     */
    public function getBip125Replaceable(): ?string
    {
        return @$this->payment->{'bip125-replaceable'};
    }

    /**
     * Indicates if a transaction is was abandoned
     *
     * @return bool|null
     */
    public function isAbandoned(): ?bool
    {
        return @$this->payment->abandoned;
    }
}

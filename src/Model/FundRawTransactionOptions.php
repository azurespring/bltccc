<?php
/**
 * Created by PhpStorm.
 * User: tsai
 * Date: 18-4-1
 * Time: 下午6:05
 */

namespace AzureSpring\Bltccc\Model;


class FundRawTransactionOptions
{
    /**
     * @var string The bitcoin address to receive the change.
     *             If not set, the address is chosen from address pool
     */
    private $changeAddress;

    /**
     * @var int The index of the change output.
     *          If not set, the change position is randomly chosen
     */
    private $changePosition;

    /**
     * @var bool Inputs from watch-only addresses are also considered.
     *           The default is false
     */
    private $includeWatching;

    /**
     * @var bool The selected outputs are locked after running the rpc call.
     *           The default is false
     */
    private $lockUnspents;

    /**
     * @var bool Reserves the change output key from the keypool.
     *           The default is true.
     */
    private $reserveChangeKey;

    /**
     * @var float The specific feerate you are willing to pay(BTC per KB).
     *            If not set, the wallet determines the fee
     */
    private $feeRate;

    /**
     * @var array A json array of integers.
     *            The fee will be equally deducted from the amount of each specified output.
     *            The outputs are specified by their zero-based index, before any change output is added.
     */
    private $subtractFeeFromOutputs;


    public function __construct()
    {
        $this->reserveChangeKey = true;
    }

    /**
     * @return string
     */
    public function getChangeAddress(): string
    {
        return $this->changeAddress;
    }

    /**
     * @param string $changeAddress
     *
     * @return $this
     */
    public function setChangeAddress(string $changeAddress): self
    {
        $this->changeAddress = $changeAddress;

        return $this;
    }

    /**
     * @return int
     */
    public function getChangePosition(): int
    {
        return $this->changePosition;
    }

    /**
     * @param int $changePosition
     *
     * @return $this
     */
    public function setChangePosition(int $changePosition): self
    {
        $this->changePosition = $changePosition;

        return $this;
    }

    /**
     * @return bool
     */
    public function isIncludeWatching(): bool
    {
        return $this->includeWatching;
    }

    /**
     * @param bool $includeWatching
     *
     * @return $this
     */
    public function setIncludeWatching(bool $includeWatching): self
    {
        $this->includeWatching = $includeWatching;

        return $this;
    }

    /**
     * @return bool
     */
    public function isLockUnspents(): bool
    {
        return $this->lockUnspents;
    }

    /**
     * @param bool $lockUnspents
     *
     * @return $this
     */
    public function setLockUnspents(bool $lockUnspents): self
    {
        $this->lockUnspents = $lockUnspents;

        return $this;
    }

    /**
     * @return bool
     */
    public function isReserveChangeKey(): bool
    {
        return $this->reserveChangeKey;
    }

    /**
     * @param bool $reserveChangeKey
     *
     * @return $this
     */
    public function setReserveChangeKey(bool $reserveChangeKey): self
    {
        $this->reserveChangeKey = $reserveChangeKey;

        return $this;
    }

    /**
     * @return float
     */
    public function getFeeRate(): float
    {
        return $this->feeRate;
    }

    /**
     * @param float $feeRate
     *
     * @return $this
     */
    public function setFeeRate(float $feeRate): self
    {
        $this->feeRate = $feeRate;

        return $this;
    }

    /**
     * @return array
     */
    public function getSubtractFeeFromOutputs(): array
    {
        return $this->subtractFeeFromOutputs;
    }

    /**
     * @param array $subtractFeeFromOutputs
     *
     * @return $this
     */
    public function setSubtractFeeFromOutputs(array $subtractFeeFromOutputs): self
    {
        $this->subtractFeeFromOutputs = $subtractFeeFromOutputs;

        return $this;
    }

    public function marshal(): array {
        $opts = [];

        if ($this->getChangeAddress()) {
            $opts['changeAddress'] = $this->getChangeAddress();
        }
        if ($this->getChangePosition()) {
            $opts['changePosition'] = $this->getChangePosition();
        }
        if ($this->isIncludeWatching()) {
            $opts['includeWatching'] = true;
        }
        if ($this->isLockUnspents()) {
            $opts['lockUnspents'] = true;
        }
        if (!$this->isReserveChangeKey()) {
            $opts['reserveChangeKey'] = false;
        }
        if ($this->getFeeRate()) {
            $opts['feeRate'] = $this->getFeeRate();
        }
        if (!empty($this->getSubtractFeeFromOutputs())) {
            $opts['subtractFeeFromOutputs'] = $this->getSubtractFeeFromOutputs();
        }

        return $opts;
    }
}

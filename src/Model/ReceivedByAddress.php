<?php

namespace AzureSpring\Bltccc\Model;

class ReceivedByAddress
{
    private $address;


    /**
     * Construct
     *
     * @param object $address
     */
    public function __construct(object $address)
    {
        $this->address = $address;
    }

    /**
     * Set to true if this address is a watch-only address
     * which has received a spendable payment
     *
     * @return bool|null
     */
    public function involvesWatchOnly(): ?bool
    {
        return @$this->address->involvesWatchonly;
    }

    /**
     * The address being described encoded in base58check
     *
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address->address;
    }

    /**
     * The total amount the address has received in bitcoins
     *
     * @return float
     */
    public function getAmount(): float
    {
        return $this->address->amount;
    }

    /**
     * The number of confirmations of the latest transaction to the address
     *
     * @return int
     */
    public function getConfirmations(): int
    {
        return $this->address->confirmations;
    }

    /**
     * The account the address belongs to
     *
     * @return string
     */
    public function getAccount(): string
    {
        return $this->address->label;
    }

    /**
     * An array of TXIDs belonging to transactions that pay the address
     *
     * @return string[]
     */
    public function getTxIds(): array
    {
        return $this->address->txids;
    }
}

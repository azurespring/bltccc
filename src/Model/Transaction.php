<?php

namespace AzureSpring\Bltccc\Model;

use AzureSpring\Bltccc\Model\TransactionDetail;

class Transaction
{
    private $tx;

    private $details;


    public function __construct(object $tx)
    {
        $this->tx = $tx;
        $this->details = array_map(
            function ($detail) {
                return new TransactionDetail($detail);
            },
            $this->tx->details
        );
    }

    /**
     * A positive number of bitcoins if this transaction increased the total wallet balance;
     * a negative number of bitcoins if this transaction decreased the total wallet balance,
     * or 0 if the transaction had no net effect on wallet balance
     *
     * @return float
     */
    public function getAmount(): float
    {
        return $this->tx->amount;
    }

    /**
     * If an outgoing transaction, this is the fee paid by the transaction reported as negative bitcoins
     *
     * @return float|null
     */
    public function getFee(): ?float
    {
        return @$this->tx->fee;
    }

    /**
     * The number of confirmations the transaction has received. Will be 0 for unconfirmed and -1 for conflicted
     *
     * @return int
     */
    public function getConfirmations(): int
    {
        return $this->tx->confirmations;
    }

    /**
     * Set to true if the transaction is a coinbase. Not returned for regular transactions
     *
     * @return bool|null
     */
    public function isGenerated(): ?bool
    {
        return @$this->tx->generated;
    }

    /**
     * The hash of the block on the local best block chain which includes this transaction,
     * encoded as hex in RPC byte order
     *
     * @return string|null
     */
    public function getBlockHash(): ?string
    {
        return @$this->tx->blockhash;
    }

    /**
     * The hash of the block on the local best block chain which includes this transaction,
     * encoded as hex in RPC byte order
     *
     * @return int|null
     */
    public function getBlockIndex(): ?int
    {
        return @$this->tx->blockindex;
    }

    /**
     * The block header time (Unix epoch time) of the block on the local best block chain which
     * includes this transaction
     *
     * @return int|null
     */
    public function getBlockTime(): ?int
    {
        return @$this->tx->blocktime;
    }

    /**
     * The TXID of the transaction, encoded as hex in RPC byte order
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->tx->txid;
    }

    /**
     * An array containing the TXIDs of other transactions that spend the same inputs (UTXOs) as
     * this transaction
     *
     * @return string[]
     */
    public function getWalletConflicts(): array
    {
        return $this->tx->walletconflicts;
    }

    /**
     * A Unix epoch time when the transaction was added to the wallet
     *
     * @return int
     */
    public function getTime(): int
    {
        return $this->tx->time;
    }

    /**
     * A Unix epoch time when the transaction was detected by the local node, or
     * the time of the block on the local best block chain that included the transaction
     *
     * @return int
     */
    public function getTimeReceived(): int
    {
        return $this->tx->timereceived;
    }

    /**
     * Indicates if a transaction is replaceable under BIP 125
     *
     * @return string yes, no or unknown
     */
    public function getBip125Replaceable(): string
    {
        return $this->tx->{'bip125-replaceable'};
    }

    /**
     * For transaction originating with this wallet, a locally-stored comment added to the transaction
     *
     * @return string|null
     */
    public function getComment(): ?string
    {
        return @$this->tx->comment;
    }

    /**
     * For transaction originating with this wallet, a locally-stored comment added to
     * the transaction identifying who the transaction was sent to
     *
     * @return string|null
     */
    public function getTo(): ?string
    {
        return @$this->tx->to;
    }

    /**
     * An array containing one object for each input or output in the transaction which
     * affected the wallet
     *
     * @return TransactionDetail[]
     */
    public function getDetails(): array
    {
        return $this->details;
    }

    /**
     * The transaction in serialized transaction format
     *
     * @return string
     */
    public function getHex(): string
    {
        return $this->tx->hex;
    }
}

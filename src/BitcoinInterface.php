<?php

namespace AzureSpring\Bltccc;

use AzureSpring\Bltccc\Model\FundRawTransactionOptions;
use AzureSpring\Bltccc\Model\Payment;
use AzureSpring\Bltccc\Model\ReceivedByAddress;
use AzureSpring\Bltccc\Model\Transaction;
use AzureSpring\Bltccc\Model\UnspentOutput;
use AzureSpring\Bltccc\Model\WalletInfo;

interface BitcoinInterface
{
    const HASH_ALL      = 'ALL';
    const HASH_NONE     = 'NONE';
    const HASH_SINGLE   = 'SINGLE';
    const HASH_ALL_ANY  = 'ALL|ANYONECANPAY';
    const HASH_NONE_ANY = 'NONE|ANYONECANPAY';
    const HASH_SINGLE_ANY = 'SINGLE|ANYONECANPAY';

    const RPC_VERIFY_ALREADY_IN_CHAIN = -27;

    /**
     * Provides information about the wallet
     *
     * @return mixed
     */
    public function getWalletInfo(): WalletInfo;

    /**
     * Encrypts the wallet with a passphrase
     *
     * @param string $passphrase the passphrase to use for the encrypted wallet (must be at least one character)
     */
    public function encryptWallet(string $passphrase);

    /**
     * Removes the wallet encryption key from memory, locking the wallet
     */
    public function walletLock();

    /**
     * Stores the wallet decryption key in memory for the indicated number of seconds
     *
     * @param string $passphrase the passphrase that unlocks the wallet
     * @param int    $timeout    the number of seconds after which the decryption key will be automatically deleted from memory
     *
     * @return $this
     */
    public function walletPassphrase(string $passphrase, int $timeout);

    /**
     * changes the wallet passphrase from ‘old passphrase’ to ‘new passphrase’
     *
     * @param string $oldPassphrase the current wallet passphrase
     * @param string $newPassphrase the new passphrase for the wallet
     */
    public function walletPassphraseChange(string $oldPassphrase, string $newPassphrase);

    /**
     * Fills the cache of unused pre-generated keys (the keypool)
     *
     * @param int $size the new size of the keypool; if the number of keys in the keypool is
     *                  less than this number, new keys will be generated
     */
    public function keyPoolRefill(int $size = 100);

    /**
     * Sets the transaction fee per kilobyte paid by transactions created by this wallet
     *
     * @param float $amount the transaction fee to pay, in bitcoins,
     *                      for each kilobyte of transaction data
     */
    public function setTxFee(float $amount);

    /**
     * Returns the name of the account associated with the given address
     *
     * @param string $address a P2PKH or P2SH Bitcoin address belonging either to a specific account or the default account (“”)
     *
     * @return string an account name
     */
    public function getAccount(string $address): string;

    /**
     * Gets the balance in decimal bitcoins across all accounts or for a particular account
     *
     * @param string $account          the name of an account to get the balance for
     * @param int    $minconf          the minimum number of confirmations
     * @param bool   $includeWatchOnly whether to include watch-only addresses
     *
     * @return float
     */
    public function getBalance(string $account, int $minconf = 1, bool $includeWatchOnly = false): float;

    /**
     * Returns the wallet’s total unconfirmed balance
     *
     * @return float
     */
    public function getUnconfirmedBalance(): float;

    /**
     * Returns a new Bitcoin address for receiving payments
     *
     * @param string $account the name of the account to put the address in
     *
     * @return string
     */
    public function getNewAddress(string $account): string;

    /**
     * Returns the total amount received by the specified address
     * in transactions with the specified number of confirmations
     *
     * @param string $address the address whose transactions should be tallied
     * @param int    $minconf the minimum number of confirmations
     *
     * @return float
     */
    public function getReceivedByAddress(string $address, int $minconf = 1): float;

    /**
     * Adds an address or pubkey script to the wallet without the associated private key
     *
     * allowing you to watch for transactions affecting that address or
     * pubkey script without being able to spend any of its outputs
     *
     * @param string $address either a P2PKH or P2SH address encoded in base58check, or a pubkey script encoded as hex
     * @param string $account an account name into which the address should be placed
     * @param bool   $rescan  set to true (the default) to rescan the entire local block database for
     *                        transactions affecting any address or pubkey script in the wallet
     * @param bool   $p2sh    add the P2SH version of the script as well
     */
    public function importAddress(string $address, string $account = '', bool $rescan = true, bool $p2sh = false);

    /**
     * Imports addresses or scripts (with private keys, public keys, or P2SH redeem scripts) and
     * optionally performs the minimum necessary rescan for all imports
     */
    public function importMulti($requests, $options);

    /**
     * Adds a public key (in hex) that can be watched as if it were in your wallet but cannot be used to spend
     *
     * @param string $pubkey
     * @param string $label
     * @param bool   $rescan
     */
    public function importPubKey(string $pubkey, string $label = '', bool $rescan = true);

    /**
     * Adds a private key to your wallet
     *
     * @param string $litecoinPrivKey the private key to import into the wallet encoded
     *                                in base58check using wallet import format (WIF)
     * @param string $account         the name of an account to which transactions
     *                                involving the key should be assigned
     * @param bool   $rescan          set to true (the default) to rescan the entire local block database for
     *                                transactions affecting any address or pubkey script in the wallet
     *                                (including transaction affecting the newly-added address for this private key)
     */
    public function importPrivKey(string $litecoinPrivKey, string $account = '', bool $rescan = true);

    /**
     * Returns the wallet-import-format (WIP) private key corresponding to an address
     *
     * @param string $address the P2PKH address corresponding to the private key you want returned
     *
     * @return string the private key encoded as base58check using wallet import format
     */
    public function dumpPrivKey(string $address): string;

    /**
     * Lists groups of addresses that may have had their common ownership
     * made public by common use as inputs in the same transaction or
     * from being used as change from a previous transaction
     */
    public function listAddressGroupings();

    /**
     * Lists the total number of bitcoins received by each address
     *
     * @param int  $minconf          the minimum number of confirmations an externally-generated transaction must have
     *                               before it is counted towards the balance
     * @param bool $includeEmpty     set to true to display accounts which have never received a payment
     * @param bool $includeWatchOnly if set to true, include watch-only addresses in details and
     *                               calculations as if they were regular addresses belonging to the wallet
     *
     * @return ReceivedByAddress[]
     */
    public function listReceivedByAddress(int $minconf = 1, bool $includeEmpty = false, bool $includeWatchOnly = false): array;

    /**
     * Gets all transactions affecting the wallet which have occurred since a particular block,
     * plus the header hash of a block at a particular depth
     *
     * @param string $blockHash           the hash of a block header encoded as hex in RPC byte order
     * @param int    $targetConfirmations sets the lastblock field of the results to
     *                                    the header hash of a block with this many confirmations
     * @param bool   $includeWatchOnly    if set to true, include watch-only addresses in details and
     *                                    calculations as if they were regular addresses belonging to the wallet
     *
     * @return Payment[]
     */
    public function listSinceBlock(string $blockHash = '', int $targetConfirmations = 1, bool $includeWatchOnly = false): array;

    /**
     * Returns the most recent transactions that affect the wallet
     *
     * @param string $account          the name of an account to get transactinos from
     * @param int    $count            the number of the most recent transactions to list
     * @param int    $skip             the number of the most recent transactions which should not be returned
     * @param bool   $includeWatchOnly if set to true, include watch-only addresses in details and calculations as if
     *                                 they were regular addresses belonging to the wallet
     *
     * @return Payment[]
     */
    public function listTransactions(string $account, int $count = 10, int $skip = 0, bool $includeWatchOnly = false): array;

    /**
     * Returns an array of unspent transaction outputs belonging to this wallet
     *
     * @param int        $minconf       the minimum number of confirmations an output must have
     * @param int        $maxconf       the maximum number of confirmations an output may have
     * @param array|null $addresses     the addresses an output must pay
     * @param bool       $includeUnsafe
     *
     * @return UnspentOutput[]
     */
    public function listUnspent(int $minconf = 1, int $maxconf = 9999999, array $addresses = null, $includeUnsafe = true);

    /**
     * Returns a list of temporarily unspendable (locked) outputs
     */
    public function listLockUnspent();

    /**
     * Temporarily locks or unlocks specified transaction outputs
     */
    public function lockUnspent(bool $unlock, array $outputs = null);

    /**
     * Creates and broadcasts a transaction which sends outputs to multiple addresses
     */
    public function sendMany(string $fromAccount, $outputs, int $minconf = 1, string $comment = null, array $subtractFeeFrom = null);

    /**
     * Spends an amount to a given address
     *
     * @param string      $address               a P2PKH or P2SH address to which the bitcoins should be sent
     * @param float       $amount                the amount to spent in bitcoins
     * @param string|null $comment               a locally-stored (not broadcast) comment assigned to this transaction
     * @param string|null $commentTo             a locally-stored (not broadcast) comment assigned to this transaction
     * @param bool        $subtractFeeFromAmount the fee will be deducted from the amount being sent
     *
     * @return string the TXID of the sent transaction, encoded as hex in RPC byte order
     */
    public function sendToAddress(string $address, float $amount, string $comment = null, string $commentTo = null, bool $subtractFeeFromAmount = false): string;

    /**
     * Gets detailed information about an in-wallet transaction
     *
     * @param string $txId             the TXID of the transaction to get details about
     * @param bool   $includeWatchOnly whether to include watch-only addresses in details and calculations
     *
     * @return Transaction
     */
    public function getTransaction(string $txId, bool $includeWatchOnly = false): Transaction;

    public function importPrunedFunds($rawTransaction, $txOutProof);

    public function removePrunedFunds($txId);

    public function signMessage($address, $message);

    /**
     * Creates an unsigned serialized transaction that spends a previous output
     * to a new output with a P2PKH or P2SH address. The transaction is not
     * stored in the wallet or transmitted to the network.
     *
     * @param array    $inputs   An array of objects, each one to be used as an input to the transaction
     * @param array    $outputs  The addresses and amounts to pay
     * @param int|null $lockTime Indicates the earliest time a transaction can be added to the block chain
     *
     * @return string
     */
    public function createRawTransaction(array $inputs, array $outputs, int $lockTime = null): string;

    /**
     * Decodes a serialized transaction hex string into a JSON object describing the transaction.
     *
     * @param string $tx
     *
     * @return mixed
     */
    public function decodeRawTransaction(string $tx);

    /**
     * Adds inputs to a transaction until it has enough in value to meet its out value.
     *
     * @param string                    $tx      The hex string of the raw transaction
     * @param FundRawTransactionOptions $options Additional options
     *
     * @return array [string $hex, float $fee, int $changePos] information about the created transaction
     */
    public function fundRawTransaction(string $tx, FundRawTransactionOptions $options = null): array;

    /**
     * Signs a transaction in the serialized transaction format using private keys
     * stored in the wallet or provided in the call.
     *
     * @param string $tx           The transaction to sign as a serialized transaction
     * @param array  $dependencies The previous outputs being spent by this transaction
     * @param array  $privateKeys  An array holding private keys
     * @param string $sigHash      The type of signature hash to use for all of the signatures performed
     *
     * @return array [string $hex, bool $complete] the transaction with any signatures made
     */
    public function signRawTransaction(string $tx, array $dependencies = null, array $privateKeys = null, string $sigHash = null): array;

    /**
     * Validates a transaction and broadcasts it.
     *
     * @param string $tx           The serialized transaction to broadcast encoded as hex
     * @param bool   $allowHighFee Set to true to allow the transaction to pay a high transaction fee.
     *                             Set to false (the default) to prevent Bitcoin Core from
     *                             broadcasting the transaction if it includes a high fee.
     *
     * @return null|string
     */
    public function sendRawTransaction(string $tx, bool $allowHighFee = false): ?string;

    /**
     * The validateaddress RPC returns information about the given Bitcoin address.
     *
     * @param string $address The P2PKH or P2SH address to validate encoded in base58check format
     *
     * @return bool Set to true if the address is a valid P2PKH or P2SH address; set to false otherwise
     */
    public function validateAddress(string $address);
}
